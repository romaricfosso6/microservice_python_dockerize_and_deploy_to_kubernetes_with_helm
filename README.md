# microservice_python_dockerize_and_deploy_to_kubernetes_with_helm

microservice-in-python in de linux environnement
dans ce projet nous allons suivre les étapes suivantes 
 - Installing python 3.x
 - Creating python Virtual Environments
 - Installing Python vs Code Extantion
 - Sample Flask Application 
 - Jinja templating for Dynamic web Pages
 - Usuing pip to freeze python Dependencies 
 - Building the docker image using Dockerfile
 - writing Docker compose file
 - Writing kubernetes manifest files for the application
 - creating helm chart 


## creating python Virtual Environments
 - install package python3.8-venv
   "sudo apt install python3.8-venv"
par la suite crée l environnement virtuel 
- python3 -m  venv devops_labe_python-env
activer l environnement
- source devops_labe_python-env/bin/activate
par la suite ctrl+d pour quitter, pour quitté on a deactivate 
